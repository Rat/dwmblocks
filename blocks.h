//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"",	"cat /tmp/recordingicon 2>/dev/null",	0,	9},
	/* {"",	"sb-music",	0,	11}, */
	{"",	"sb-pacpackages",	0,	8},
	{"",	"sb-news",		0,	6},
	/* {"",	"sb-crypto",	0,	13}, */
	/* {"",	"price bat \"Basic Attention Token\" 🦁",	0,	20}, */
	/* {"",	"price btc Bitcoin 💰",				0,	21}, */
	/* {"",	"price lbc \"LBRY Token\" 📚",			0,	22}, */
	{"",	"sb-torrent",	20,	7},
	//{"",	"sb-memory",	10,	14},
	//{"",	"sb-cpu",	10,	18},
	{" ",	"sb-disk",	60, 19},
	/* {"",	"sb-moonphase",	18000,	17}, */
	{" ",	"sb-forecast",	18000,	5},
	//{"",	"sb-mailbox",	180,	12},
	/* {"",	"sb-nettraf",	1,	16}, */
	{" ",	"sb-volume",	0,	10},
	/* {"",	"sb-battery",	5,	3}, */
	{" ",	"sb-clock",	30,	1},
	{" ",	"sb-internet",	60,	4},
//	{"",	"sb-help-icon",	0,	15},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "";
static unsigned int delimLen = 5;
